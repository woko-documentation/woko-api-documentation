stages:
  - fetch-doc
  - build
  - deploy

variables:
  AWS_REGION: us-west-2
  API_GATEWAY_PROFILE: woko
  REST_API_ID: 6075m77sq7
  STAGE_NAME: prod
  EXPORT_TYPE: oas30
  EXPORT_PARAMETERS: 'extensions=postman'
  API_DOC_FILENAME: woko-api-doc.yaml

# Fetch API documentation from AWS API Gateway
fetch-doc:
  stage: fetch-doc
  image: node:latest
  before_script:
    - apt-get update && apt-get install -y awscli
  script:
    - 'echo "Fetching API Gateway documentation..."'
    - 'aws apigateway get-export --rest-api-id "$REST_API_ID" --stage-name "$STAGE_NAME" --export-type "$EXPORT_TYPE" --region "$AWS_REGION" --parameters "$EXPORT_PARAMETERS" --accepts "application/yaml" "$API_DOC_FILENAME"'
  artifacts:
    paths:
      - woko-api-doc.yaml

# Build phase that updates the OpenAPI spec and generates the architecture diagram
build:
  stage: build
  image: node:latest  # Use Node image for this stage since npm is required
  before_script:
    # Install necessary tools and create a virtual environment for Python dependencies
    - apt-get update && apt-get install -y python3 python3-pip python3-venv graphviz
    - python3 -m venv venv  # Create a virtual environment
    - source venv/bin/activate  # Activate the virtual environment
    - pip install --upgrade pip  # Ensure pip is up to date
    - pip install diagrams pyyaml  # Install necessary Python packages in the virtual environment
  script:
    # Generate architecture diagram image
    - python3 diagram.py

    # Move the image to the public folder for GitLab Pages
    - mkdir -p public
    - mv woko_api_-_architecture_overview.png public/

    # Inject markdown description into OpenAPI spec
    - python3 update_openapi.py

    # Install redoc-cli for generating HTML documentation
    - npm install -g redoc-cli

    # Generate the HTML documentation using Redoc

    - redoc-cli bundle woko-api-doc.yaml -o public/index.html
    
  artifacts:
    paths:
      - public
      - woko_api_-_architecture_overview.png  # Ensure the generated diagram is saved as an artifact

# Deploy phase for GitLab Pages
pages:
  stage: deploy
  image: node:latest  # Use Node image for this stage
  script:
    - echo "Deploying to GitLab Pages..."
  artifacts:
    paths:
      - public
  only:
    - main
