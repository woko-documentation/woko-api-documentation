# Woko API Overview

This API manages user registrations, logins, and credit requests for the Woko platform.

## Architecture

Below is the architecture of the Woko system, showing the interaction between AWS services:

![Woko API Architecture Overview](woko_api_-_architecture_overview.png)

The diagram above represents the main components involved, including API Gateway, Lambda functions, DynamoDB, and Cognito for authentication.
