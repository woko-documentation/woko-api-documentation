import yaml
import os

# File paths
openapi_file = "woko-api-doc.yaml"
markdown_file = "api-description.md"

# Load OpenAPI spec
def load_openapi_spec(filepath):
    with open(filepath, 'r') as file:
        return yaml.safe_load(file)

# Inject markdown into OpenAPI
def inject_markdown_into_openapi(openapi_spec, markdown_content):
    if 'info' in openapi_spec:
        openapi_spec['info']['description'] = markdown_content
    return openapi_spec

# Add tags based on path prefixes
def add_tags_to_openapi(openapi_spec):
    paths = openapi_spec.get('paths', {})

    # Define tag mappings based on path prefixes
    tag_mapping = {
        '/credit-management': 'Credit Management',
        '/user-management': 'User Management'
    }
    
    # Default tag for any paths that don't match the prefixes
    default_tag = 'Others'

    # Iterate over paths and add corresponding tags based on prefixes
    for path, path_item in paths.items():
        for method, operation in path_item.items():
            if method in ['get', 'post', 'put', 'patch', 'delete']:  # Only process HTTP methods
                # Assign a tag based on matching path prefix, or default tag
                matched_tag = default_tag
                for prefix, tag in tag_mapping.items():
                    if path.startswith(prefix):
                        matched_tag = tag
                        break  # Stop once we find the matching prefix
                operation['tags'] = [matched_tag]
    
    return openapi_spec

# Add x-tagGroups to group tags in the documentation
def add_tag_groups(openapi_spec):
    # Define the x-tagGroups structure for Redocly
    tag_groups = [
        {
            'name': 'Credit Management',
            'tags': ['Credit Management']
        },
        {
            'name': 'User Management',
            'tags': ['User Management']
        },
        {
            'name': 'Others',
            'tags': ['Others']
        }
    ]
    
    openapi_spec['x-tagGroups'] = tag_groups
    return openapi_spec

# Save modified OpenAPI spec
def save_openapi_spec(filepath, openapi_spec):
    with open(filepath, 'w') as file:
        yaml.dump(openapi_spec, file, sort_keys=False)

# Main execution
if __name__ == "__main__":
    # Check if files exist
    if not os.path.exists(openapi_file):
        print(f"{openapi_file} not found.")
        exit(1)
    if not os.path.exists(markdown_file):
        print(f"{markdown_file} not found.")
        exit(1)

    # Load OpenAPI spec and markdown content
    openapi_spec = load_openapi_spec(openapi_file)
    with open(markdown_file, 'r') as md_file:
        markdown_content = md_file.read()

    # Inject markdown into OpenAPI
    updated_openapi_spec = inject_markdown_into_openapi(openapi_spec, markdown_content)

    # Add tags to OpenAPI based on path prefixes
    updated_openapi_spec = add_tags_to_openapi(updated_openapi_spec)

    # Add x-tagGroups for grouping in Redocly
    updated_openapi_spec = add_tag_groups(updated_openapi_spec)

    # Save the updated OpenAPI spec
    save_openapi_spec(openapi_file, updated_openapi_spec)

    print(f"Successfully updated {openapi_file} with markdown, tags, and tag groups")
