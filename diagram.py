from diagrams import Diagram, Edge, Cluster
from diagrams.aws.network import APIGateway, Route53
from diagrams.aws.compute import Lambda
from diagrams.aws.security import Cognito
from diagrams.aws.storage import SimpleStorageServiceS3
from diagrams.aws.database import Dynamodb
from diagrams.aws.management import Cloudwatch
from diagrams.aws.integration import SNS
from diagrams.onprem.client import Users
from diagrams.onprem.client import Client  # Replacing mobile app with client

with Diagram("Woko API - Architecture Overview", show=False):

    # External users and mobile app interacting with the system
    users = Users("End Users")
    mobile_app = Client("Mobile App")

    # Core services
    route53 = Route53("Route 53 DNS")
    api_gw = APIGateway("API Gateway")
    cloudwatch = Cloudwatch("CloudWatch Logs")
    
    # Move SNS and S3 to the top for better flow
    with Cluster("Email System"):
        s3 = SimpleStorageServiceS3("Email Templates/Assets S3 Bucket")
        sns = SNS("SNS (Email Notification)")
        
    # Adjusted layout for user-management module
    with Cluster("User Management Module"):
        cognito = Cognito("Cognito (User Auth)")
        
        # Move Send Email Confirmation to the top
        send_email = Lambda("Send Email Confirmation")
        
        # Horizontally aligned user management lambdas
        register_user = Lambda("Register User")
        verify_user = Lambda("Verify User")
        login_user = Lambda("Login User")
        forgot_pass = Lambda("Forgot Password")
        reset_pass = Lambda("Reset Password")
        
        # User Management Lambdas call Cognito directly
        [register_user, verify_user, login_user, forgot_pass, reset_pass] >> Edge(label="Uses Cognito API") >> cognito

        # S3 interaction for fetching email templates/assets
        send_email >> Edge(label="Fetches Email Templates/Assets") >> s3

        # SNS interaction for sending emails
        send_email >> Edge(label="Sends Emails") >> sns

    # Adjusted layout for credit-request management module
    with Cluster("Credit Request Management Module"):
        credit_req = Lambda("Credit Request")
        create_client = Lambda("Create Client")
        
        # DynamoDB tables for credit management
        dynamo_credit_req = Dynamodb("Credit Request Table")
        dynamo_create_client = Dynamodb("Client Table")

    # User interactions
    users >> Edge(label="Interacts with") >> mobile_app
    mobile_app >> Edge(label="API Requests") >> api_gw
    route53 >> Edge(label="Routes traffic") >> api_gw

    # API Gateway routes to Lambda functions
    api_gw >> Edge(label="POST /register-user") >> register_user
    api_gw >> Edge(label="GET /verify-user") >> verify_user
    api_gw >> Edge(label="POST /login-user") >> login_user
    api_gw >> Edge(label="POST /forgot-password") >> forgot_pass
    api_gw >> Edge(label="POST /reset-password") >> reset_pass
    api_gw >> Edge(label="POST /credit-request") >> credit_req
    api_gw >> Edge(label="POST /create-client") >> create_client
    api_gw >> Edge(label="POST /send-email") >> send_email

    # Credit Request Management Module interacts with its own DynamoDB tables
    credit_req >> Edge(label="Stores Data") >> dynamo_credit_req
    create_client >> Edge(label="Stores Data") >> dynamo_create_client

    # Monitoring
    cloudwatch << Edge(label="Logs Events") << api_gw
